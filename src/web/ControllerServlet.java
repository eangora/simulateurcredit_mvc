package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.CreditMetier;

public class ControllerServlet  extends HttpServlet{
	
	//On fait appel � la class Metier
	private CreditMetier metier;
	
	//instanciation de la methode init()
	@Override
	public void init() throws ServletException {
		metier = new CreditMetier();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) 
			throws ServletException, IOException {
		request.setAttribute("mod", new CreditModel());
		request.getRequestDispatcher("VueCredit.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest req,
			HttpServletResponse resp) 
			throws ServletException, IOException {
		//On fait un caste en double gteparameter retourne un string
		double montant = Double.parseDouble(req.getParameter("montant"));
		double taux = Double.parseDouble(req.getParameter("taux"));
		int duree = Integer.parseInt(req.getParameter("duree"));
		//En utilsant un contstrusteur avec param�tre �a �vite de d'�crire model.set
		CreditModel model = new CreditModel();
		model.setMontant(montant);
		model.setTaux(taux);
		model.setDuree(duree);
		double res = metier.calculMensualite(montant, duree, taux);
		model.setMensualite(res);
		//Avant d'acceder � la vue on passera par setAttribut on stock le model dans la vue request
		req.setAttribute("mod", model);
		req.getRequestDispatcher("VueCredit.jsp").forward(req, resp);
		
		//On peut faire la v�rification de la validation de donn�es
		
	
	}
}
